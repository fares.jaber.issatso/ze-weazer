import { TempUnit } from "../types";

export const convertTemperature = (temp: number, to: TempUnit) => {
  let convertedTemp = temp;
  switch (to) {
    case "K":
      convertedTemp = temp;
      break;
    case "C":
      convertedTemp = temp - 273.15;
      break;
    case "F":
      convertedTemp = ((temp - 273.15) * 9) / 5 + 32;
      break;
  }
  return Number(convertedTemp.toFixed(0));
};
