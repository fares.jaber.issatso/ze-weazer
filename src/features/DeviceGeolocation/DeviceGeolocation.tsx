import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarked } from "@fortawesome/free-solid-svg-icons";
import { useDispatch } from "react-redux";
import { resetLocation } from "../../store/actions/locationActions";

const DeviceGeolocation = () => {
  const dispatch = useDispatch();
  return (
    <GeolocationButton
      title="Locate me"
      onClick={() => {
        dispatch(resetLocation());
      }}
    >
      <FontAwesomeIcon icon={faMapMarked} />
    </GeolocationButton>
  );
};

export default DeviceGeolocation;

const GeolocationButton = styled.button`
  background-color: #fff;
  position: fixed;
  z-index: 6;
  top: 120px;
  left: 50px;
  padding: 15px;
  border-radius: 50%;
  border: none;
  cursor: pointer;
  outline: none;
`;
