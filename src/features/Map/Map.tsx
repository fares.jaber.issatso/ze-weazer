import React, { useContext, useEffect } from "react";
import GoogleMapReact, { Coords } from "google-map-react";
import { useState } from "react";
import useDeviceGeolocation from "../../hooks/useDeviceGeolocation.hook";
import axios, { AxiosResponse } from "axios";
import { TempUnit, WeatherEntityType, WeatherResponseType } from "../../types";
import { useDispatch, useSelector } from "react-redux";
import { setWeatherData } from "../../store/actions/weatherActions";
import { WeatherReducerType } from "../../store/reducers/weatherReducer";
import styled from "styled-components";
import { convertTemperature } from "../../commun/utils";
import { customMapStyles } from "./mapStyles";
import { LocationReducerType } from "../../store/reducers/locationReducer";
import { setLocation } from "../../store/actions/locationActions";

const Map = () => {
  const geolocation = useDeviceGeolocation();
  const dispatch = useDispatch();

  const [zoomLevel, setZoomLevel] = useState(0);
  const weather = useSelector(
    (state: { weatherReducer: WeatherReducerType }) => state.weatherReducer
  );
  const location = useSelector(
    (state: { locationReducer: LocationReducerType }) => state.locationReducer
  );

  useEffect(() => {
    if (!geolocation && !location) return;
    setZoomLevel(9);
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/onecall?lat=${
          location.lat || geolocation.lat
        }&lon=${location.lng || geolocation?.lng}&appid=${
          process.env.REACT_APP_WEATHER_API_KEY
        }`
      )
      .then((res: AxiosResponse<WeatherResponseType>) =>
        dispatch(setWeatherData(res.data))
      );
  }, [dispatch, geolocation, location]);
  return (
    <div style={{ height: "100vh", width: "100%" }}>
      <GoogleMapReact
        bootstrapURLKeys={{
          key: process.env.REACT_APP_GMAPS_KEY
            ? process.env.REACT_APP_GMAPS_KEY
            : "",
        }}
        defaultCenter={{ lat: 0, lng: 0 }}
        center={
          !!location.lat && !!location.lng ? (location as Coords) : geolocation
        }
        defaultZoom={0}
        zoom={zoomLevel}
        options={{ styles: customMapStyles, maxZoom: 9 }}
        onZoomAnimationEnd={(zoom) => setZoomLevel(zoom)}
        onClick={(e) => {
          dispatch(setLocation(e.lat, e.lng));
        }}
      >
        {(location || geolocation) && weather.current && (
          <WeatherMarker
            lat={location.lat || geolocation.lat}
            lng={location.lng || geolocation.lng}
            weather={weather.current}
            unit={weather.unit}
          />
        )}
      </GoogleMapReact>
    </div>
  );
};

export default Map;

const WeatherMarker = ({
  weather,
  unit,
}: {
  lat: number;
  lng: number;
  weather: WeatherEntityType;
  unit: TempUnit;
}) => {
  return (
    <WeatherMarkerWrapper>
      <div
        className="weather-icon"
        style={{
          backgroundImage: `url(http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png)`,
        }}
      ></div>
      <div className="temp">
        <div className="content">
          <span className="value">
            {convertTemperature(
              typeof weather.temp === "number" ? weather.temp : 0,
              unit
            )}
          </span>
          <span className="unit">{unit}</span>
        </div>
      </div>
    </WeatherMarkerWrapper>
  );
};

const WeatherMarkerWrapper = styled.div`
  display: grid;
  grid-template-columns: 40px 1fr;
  grid-template-areas: "icon temp";
  gap: 5px;
  transform: translate(-50px, -20px);

  .weather-icon {
    grid-area: icon;
    height: 40px;
    width: 40px;
    background-position: center;
    background-size: 160% 160%;
  }

  .temp {
    grid-area: temp;
    display: flex;
    flex-direction: column;
    justify-content: center;
    .content {
      .value {
        font-size: 35px;
        font-weight: 800;
        color: goldenrod;
      }
      .unit {
        font-size: 20px;
        font-weight: 100;
        color: goldenrod;
      }
    }
  }
`;
