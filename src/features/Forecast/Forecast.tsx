import React, { useContext } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { convertTemperature } from "../../commun/utils";
import { WeatherReducerType } from "../../store/reducers/weatherReducer";
import Moment from "react-moment";

const Forecast = () => {
  const { forecast, unit } = useSelector(
    (state: { weatherReducer: WeatherReducerType }) => state.weatherReducer
  );
  return (
    <ForecastWrapper>
      {forecast?.map((wf, i) => (
        <div className="forecast" key={i}>
          <div className="date">
            <Moment format={"DD MMMM"}>
              {new Date().setDate(new Date().getDate() + i + 1)}
            </Moment>
          </div>
          <div className="weather">
            <div className="icon">
              <img
                src={`http://openweathermap.org/img/wn/${wf.weather[0].icon}@2x.png`}
                alt=""
              />
            </div>
            <div className="temp">
              <div className="content">
                <span className="value">
                  {convertTemperature(
                    typeof wf.temp === "number" ? wf.temp : wf.temp.day,
                    unit
                  )}
                </span>
                <span className="unit">{unit}</span>
              </div>
            </div>
            <div className="temp">
              <div className="content">
                <span className="value">
                  {convertTemperature(
                    typeof wf.temp === "number" ? wf.temp : wf.temp.night,
                    unit
                  )}
                </span>
                <span className="unit">{unit}</span>
              </div>
            </div>
          </div>
        </div>
      ))}
    </ForecastWrapper>
  );
};

export default Forecast;

const ForecastWrapper = styled.div`
  position: fixed;
  z-index: 5;
  bottom: 50px;
  left: 100px;
  right: 100px;
  padding: 20px;
  border-radius: 20px;
  background-color: #fff;
  text-align: center;
  justify-content: space-evenly;

  display: flex;
  overflow: auto;

  .forecast {
    .temp {
      grid-area: temp;
      display: flex;
      flex-direction: column;
      justify-content: center;
      .content {
        .value {
          font-size: 23px;
          font-weight: 800;
          color: goldenrod;
        }
        .unit {
          font-size: 17px;
          font-weight: 100;
          color: goldenrod;
        }
      }
    }
  }

  @media (max-width: 1024px) {
    justify-content: unset;
  }
`;
