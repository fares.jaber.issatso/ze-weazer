import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import React, { useState } from "react";
import styled from "styled-components";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import { useDispatch } from "react-redux";
import { setLocation } from "../../store/actions/locationActions";

const PlaceSelector = () => {
  const [inputExpended, setInputExpended] = useState(false);
  const [address, setAddress] = useState("");
  const [inputRef, setInputRef] = useState<HTMLInputElement>();
  const dispatch = useDispatch();

  const handleChange = (address: string) => {
    setAddress(address);
  };

  const handleSelect = (address: string) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        dispatch(setLocation(latLng.lat, latLng.lng));
        setAddress("");
        setInputExpended(false);
      });
  };
  return (
    <PlaceSelectorWrapper expended={inputExpended}>
      <div className="input-group">
        <div className="input">
          <PlacesAutocomplete
            value={address}
            onChange={handleChange}
            onSelect={handleSelect}
          >
            {({
              getInputProps,
              suggestions,
              getSuggestionItemProps,
              loading,
            }) => (
              <div style={{ position: "relative" }}>
                <input
                  {...getInputProps({
                    placeholder: "Search Places ...",
                    className: "location-search-input",
                  })}
                  ref={(ref) => setInputRef(ref || undefined)}
                />
                <div className="autocomplete-dropdown-container">
                  {loading && <div>Loading...</div>}
                  {suggestions.map((suggestion) => {
                    const className = suggestion.active
                      ? "suggestion-item--active"
                      : "suggestion-item";
                    // inline style for demonstration purpose
                    const style = suggestion.active
                      ? { backgroundColor: "#d0d0d0", cursor: "pointer" }
                      : { backgroundColor: "#ffffff", cursor: "pointer" };
                    return (
                      <div
                        {...getSuggestionItemProps(suggestion, {
                          className,
                          style,
                        })}
                        key={suggestion.id}
                      >
                        <span>{suggestion.description}</span>
                      </div>
                    );
                  })}
                </div>
              </div>
            )}
          </PlacesAutocomplete>
        </div>
        <div
          className="search-icon"
          onClick={() => {
            setInputExpended(true);
            inputRef?.focus();
          }}
        >
          <FontAwesomeIcon className="icon" icon={faSearch} size="sm" />
        </div>
      </div>
    </PlaceSelectorWrapper>
  );
};

export default PlaceSelector;

const PlaceSelectorWrapper = styled.div<{ expended: boolean }>`
  position: fixed;
  top: 190px;
  left: 50px;
  z-index: 5;
  display: flex;
  flex-direction: row;
  .input-group {
    display: flex;
    flex-direction: row;
    padding: 10px;
    background-color: #fff;
    z-index: 10;
    border-radius: 50px;
    .input {
      width: ${({ expended }) => (expended ? "150px" : "0")};
      overflow: hidden;
      transition: all 0.5s ease;
      z-index: 10;
      input {
        border: none;
        border-bottom: 1px solid #e1e1e1;
        padding: 5px 15px;
        outline: none;
      }
      .autocomplete-dropdown-container {
        position: fixed;
        z-index: 8;
        top: 250px;
        width: 200px;
        transform: translateX(-10px);
      }
    }
  }
  .search-icon {
    display: flex;
    flex-direction: column;
    justify-content: center;
    cursor: pointer;
    .icon {
      width: 25px;
    }
  }
`;
