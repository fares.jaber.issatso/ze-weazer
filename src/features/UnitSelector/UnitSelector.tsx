import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { setUnit } from "../../store/actions/weatherActions";
import { WeatherReducerType } from "../../store/reducers/weatherReducer";
import { TempUnit } from "../../types";

const UnitSelector = () => {
  const { unit } = useSelector(
    (state: { weatherReducer: WeatherReducerType }) => state.weatherReducer
  );
  const dispatch = useDispatch();
  const [isSelecting, setIsSelecting] = useState(false);

  const units: TempUnit[] = ["K", "C", "F"];

  return (
    <Selector>
      {units
        .filter((u) => u !== unit)
        .map((unit, index) => (
          <div
            key={index}
            className={`choise${isSelecting ? " selecting" : ""}`}
            onClick={() => {
              dispatch(setUnit(unit));
              setIsSelecting(false);
            }}
          >
            {unit}
          </div>
        ))}
      <div
        className="choise selected"
        onClick={() => {
          setIsSelecting(!isSelecting);
        }}
      >
        {unit}
      </div>
    </Selector>
  );
};

export default UnitSelector;

const Selector = styled.div`
  background-color: #fff;
  position: fixed;
  z-index: 6;
  width: 45px;
  height: 45px;
  text-align: center;

  top: 50px;
  left: 50px;
  border-radius: 50%;
  cursor: pointer;

  .choise {
    position: absolute;
    top: 8px;
    left: 7px;
    background: white;
    width: 30px;
    height: 30px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    text-align: center;
    border-radius: 50%;
    transition: all 0.5s ease;
    &.selecting:first-child {
      left: 55px;
    }
    &.selecting:nth-child(2) {
      left: 95px;
    }
  }
`;
