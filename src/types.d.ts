export type WeatherEntityType = {
  dt: number;
  temp: number | { max: number; min: number; day: number; night: number };
  weather: {
    main: string;
    description: string;
    icon: string;
  }[];
};

export type WeatherResponseType = {
  current: WeatherEntityType;
  daily: WeatherEntityType[];
};

export type TempUnit = "K" | "F" | "C";
