import React from "react";
import Map from "./features/Map/Map";
import DeviceGeolocation from "./features/DeviceGeolocation/DeviceGeolocation";
import PlaceSelector from "./features/PlaceSelector/PlaceSelector";
import "./index.scss";
import Forecast from "./features/Forecast/Forecast";
import UnitSelector from "./features/UnitSelector/UnitSelector";

function App() {
  return (
    <>
      <UnitSelector />
      <PlaceSelector />
      <DeviceGeolocation />
      <Map />
      <Forecast />
    </>
  );
}

export default App;
