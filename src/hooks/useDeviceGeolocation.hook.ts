import { useEffect, useState } from "react";

const useDeviceGeolocation = () => {
  const [deviceGeolocation, setDeviceGeolocation] = useState<{
    lat: number;
    lng: number;
  }>({ lat: 48.8566, lng: 2.3522 });
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setDeviceGeolocation({
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      });
    });
  }, []);
  return deviceGeolocation;
};

export default useDeviceGeolocation;
