import { LOCATION_CHANGED, LOCATION_RESET } from "../constants";
import {
  LocationReducerActionType,
  LocationReducerResetActionType,
} from "../reducers/locationReducer";

export function setLocation(
  lat: number,
  lng: number
): LocationReducerActionType {
  return {
    type: LOCATION_CHANGED,
    location: {
      lat,
      lng,
    },
  };
}

export function resetLocation(): LocationReducerResetActionType {
  return {
    type: LOCATION_RESET,
  };
}
