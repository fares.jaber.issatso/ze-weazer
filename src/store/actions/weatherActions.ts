import { TempUnit, WeatherResponseType } from "../../types";
import { WEATHER_CHANGED, UNIT_CHANGED } from "../constants";
import {
  UnitChangedActionType,
  WeatherChangedActionType,
} from "../reducers/weatherReducer";

export function setWeatherData(
  weather: WeatherResponseType
): WeatherChangedActionType {
  return {
    type: WEATHER_CHANGED,
    weather,
  };
}

export function setUnit(unit: TempUnit): UnitChangedActionType {
  return { type: UNIT_CHANGED, unit };
}
