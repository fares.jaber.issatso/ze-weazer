import { LOCATION_CHANGED, LOCATION_RESET } from "../constants";

export type LocationReducerType = {
  lat: number | null;
  lng: number | null;
};
export type LocationReducerActionType = {
  type: typeof LOCATION_CHANGED;
  location: {
    lat: number;
    lng: number;
  };
};

export type LocationReducerResetActionType = {
  type: typeof LOCATION_RESET;
};

const initState: LocationReducerType = {
  lat: null,
  lng: null,
};

const locationReducer = function (
  state = initState,
  action: LocationReducerActionType | LocationReducerResetActionType
) {
  switch (action.type) {
    case LOCATION_CHANGED:
      return { lat: action.location.lat, lng: action.location.lng };
    case LOCATION_RESET:
      return initState;
    default:
      return state;
  }
};

export default locationReducer;
