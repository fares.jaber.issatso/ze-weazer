import { TempUnit, WeatherEntityType, WeatherResponseType } from "../../types";
import { UNIT_CHANGED, WEATHER_CHANGED } from "../constants";

export type WeatherChangedActionType = {
  type: typeof WEATHER_CHANGED;
  weather: WeatherResponseType;
};

export type UnitChangedActionType = {
  type: typeof UNIT_CHANGED;
  unit: TempUnit;
};

export type WeatherReducerType = {
  current: WeatherEntityType | null;
  forecast: WeatherEntityType[] | null;
  unit: TempUnit;
};

const initState: WeatherReducerType = {
  current: null,
  forecast: null,
  unit: "C",
};

const weatherReducer = function (
  state = initState,
  action: WeatherChangedActionType | UnitChangedActionType
) {
  switch (action.type) {
    case WEATHER_CHANGED:
      return {
        ...state,
        current: action.weather.current,
        forecast: action.weather.daily,
      };
    case UNIT_CHANGED:
      return { ...state, unit: action.unit };
    default:
      return state;
  }
};

export default weatherReducer;
